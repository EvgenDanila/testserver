from flask import Flask

from config import Config
from .models import *
from .db import db, migrate


app = Flask(__name__)
app.config.from_object(Config)

db.init_app(app)
migrate.init_app(app)
with app.test_request_context():
    db.create_all()

from .routes import *
