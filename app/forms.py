from flask_wtf import FlaskForm
from wtforms import StringField, validators


class ContactForm(FlaskForm):
    name = StringField(label='Name', validators=[validators.Length(min=3, max=20)])
    surname = StringField(label='Surname', validators=[validators.Length(min=3, max=20)])
    email = StringField(label='Email', validators=[
        validators.Length(min=10, max=35),
        validators.Email()
    ])
    nickname = StringField(label='Nickname', validators=[validators.Length(min=3, max=20)])
    phone = StringField()
