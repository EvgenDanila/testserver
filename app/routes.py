from app import app
from flask import jsonify, make_response, request
from .forms import ContactForm
from .models import User


@app.route('/')
@app.route('/index')
def index():
    return 'Hello, booklovers'


@app.route('/book', methods=['GET'])
def get_books():
    return jsonify({'book': book})


@app.route('/contact', methods=['GET', 'POST'])
def contact():
    form = ContactForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User(form.name.data, form.surname.data, form.email.data, form.phone.data)
        db_session.add(user)


@app.route('/user/<name>')
def show_user(name):
    user = User.query.filter_by(name=name).first_or_404()
    return user


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(debug=True)
