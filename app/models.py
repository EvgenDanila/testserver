from .db import db


ROLE_USER = 0
ROLE_ADMIN = 1


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    surname = db.Column(db.String(80), nullable=False)
    nickname = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(128))
    phone = db.Column(db.String(10), unique=True, nullable=False)
    logo = db.Column(db.String(30))
    role = db.Column(db.SmallInteger, default=ROLE_USER)
    user_deal = db.relationship('deal', secondary=user_deal, backref=db.backref('user', lazy=True))

    def __repr__(self):
        return '<User %r>' % self.name


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), nullable=False)
    book_real = db.relationship('BookReal', backref=db.backref('book', lazy=True))
    author_book = db.relationship('author', secondary=author_book, backref=db.backref('book', lazy=True))

    def __repr__(self):
        return '<Book %r>' % self.title


class Author(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    author_book = db.relationship('book', secondary=author_book, backref=db.backref('author', lazy=True))


author_book = db.Table(
    'author_book',
    db.Column('author_id', db.Integer, db.ForeignKey('author.id'), primary_key=True),
    db.Column('book_id', db.Integer, db.ForeignKey('book.id'), primary_key=True)
)


class Publisher(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)

    def __repr__(self):
        return '<Publisher %r>' % self.name


class BookReal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    book_id = db.Column(db.Integer, db.ForeignKey('book.id'))
    publisher_id = db.Column(db.Integer, db.ForeignKey('publisher.id'))
    photo = db.Column(db.String(30))
    quality = db.Column(db.Integer, nullable=False)
    # year = db.Column(db.date)


user_deal = db.Table(
    'user_deal',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('deal_id', db.Integer, db.ForeignKey('deal.id'), primary_key=True)
)


class Deal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_deal = db.relationship('user', secondary=user_deal, backref=db.backref('deal', lazy=True))
    book_real_id = db.Column(db.Integer, db.ForeignKey('book_real.id'))
    operation_id = db.Column(db.Integer, db.ForeignKey('operation.id'))


class Operation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    operation = db.Column(db.String(80), nullable=False)
